#!/bin/sh
sudo apt-get install wget zip unzip make default-jdk

rm -r ./packages/peeragogy-monthly-wrap-generator-1/
mkdir -p ./packages/peeragogy-monthly-wrap-generator-1/
cp -r ./peeragogy-monthly-wrap-generator-1/ ./packages/

cd ./packages/
cd ./peeragogy-monthly-wrap-generator-1/

printf "Build date: $(date "+%Y-%m-%d").\n" > version.txt
currentDate=$(date "+%Y%m%d")

cd ..
cd ..


cd ./peeragogy-monthly-wrap-generator-1/
wget https://gitlab.com/publishing-systems/automated_digital_publishing/-/archive/master/automated_digital_publishing-master.zip
unzip automated_digital_publishing-master.zip
mv ./automated_digital_publishing-master/ ./automated_digital_publishing/
cd ./automated_digital_publishing/
make
cd workflows
java setup1
cd ..
cd ..
cd ..

cd ./peeragogy-monthly-wrap-generator-1/
wget https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/-/archive/master/digital_publishing_workflow_tools-master.zip
unzip digital_publishing_workflow_tools-master.zip
mv ./digital_publishing_workflow_tools-master/ ./digital_publishing_workflow_tools/
cd ./digital_publishing_workflow_tools/
make
cd workflows
cd setup
cd setup_1
java setup_1
cd ..
cd ..
cd ..
cd ..
cd ..


mkdir -p ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/
cp -r ./peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/ ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/workflows/resource_retriever/

mkdir -p ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/https_client/https_client_1/
cp -r ./peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/https_client/https_client_1/ ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/https_client/

mkdir -p ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/http_client/http_client_1/
cp -r ./peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/http_client/http_client_1/ ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/http_client/

mkdir -p ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/text_concatenator/text_concatenator_1/
cp -r ./peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/text_concatenator/text_concatenator_1/ ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/text_concatenator/

mkdir -p ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/workflows/xhtml_to_latex/xhtml_to_latex_1/
cp -r ./peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/workflows/xhtml_to_latex/xhtml_to_latex_1/ ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/workflows/xhtml_to_latex/

mkdir -p ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/latex/xhtml_prepare_for_latex/xhtml_prepare_for_latex_1/
cp -r ./peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/latex/xhtml_prepare_for_latex/xhtml_prepare_for_latex_1/ ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/latex/xhtml_prepare_for_latex/

mkdir -p ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r ./peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ ./packages/peeragogy-monthly-wrap-generator-1/digital_publishing_workflow_tools/xml_xslt_transformator/

mkdir -p ./packages/peeragogy-monthly-wrap-generator-1/automated_digital_publishing/txtreplace/txtreplace1/
cp -r ./peeragogy-monthly-wrap-generator-1/automated_digital_publishing/txtreplace/txtreplace1/ ./packages/peeragogy-monthly-wrap-generator-1/automated_digital_publishing/txtreplace/

mkdir -p ./packages/peeragogy-monthly-wrap-generator-1/automated_digital_publishing/html2epub/html2epub2/
cp -r ./peeragogy-monthly-wrap-generator-1/automated_digital_publishing/html2epub/html2epub2/ ./packages/peeragogy-monthly-wrap-generator-1/automated_digital_publishing/html2epub/


cd ./packages/

zip -r ./peeragogy-monthly-wrap-generator-1_$currentDate.zip peeragogy-monthly-wrap-generator-1
sha256sum ./peeragogy-monthly-wrap-generator-1_$currentDate.zip > ./peeragogy-monthly-wrap-generator-1_$currentDate.zip.sha256
rm -r ./peeragogy-monthly-wrap-generator-1/

cd ..
