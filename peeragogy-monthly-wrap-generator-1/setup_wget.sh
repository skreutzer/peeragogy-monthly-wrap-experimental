#!/bin/sh
sudo apt-get install default-jre pandoc unzip wget texlive-xetex texlive-latex-recommended texlive-latex-extra fonts-linuxlibertine texlive-fonts-recommended

wget https://github.com/Tufte-LaTeX/tufte-latex/archive/master.zip
unzip ./master.zip
mv ./tufte-latex-master/ ./tufte-latex/
rm master.zip

sleep 3s; wget https://github.com/edwardtufte/tufte-css/archive/gh-pages.zip
unzip ./gh-pages.zip
mv ./tufte-css-gh-pages/ ./tufte-css/
rm ./gh-pages.zip

#sleep 3s; wget https://github.com/Peeragogy/PeeragogyMonthlyWrap/archive/master.zip
#unzip ./master.zip
#mv ./PeeragogyMonthlyWrap-master/ ./PeeragogyMonthlyWrap/
#rm ./master.zip

mkdir ./source/
mkdir ./media/

wget -P ./source/ -O ./source/resource_0 https://raw.githubusercontent.com/wiki/Peeragogy/Peeragogy.github.io/Monthly-Wrap:-December-2018-by-Charlie.md
sleep 3s; wget -P ./source/ -O ./source/resource_1 https://raw.githubusercontent.com/wiki/Peeragogy/Peeragogy.github.io/Monthly-Wrap:-March-2019-by-Charlie.md
sleep 3s; wget -P ./source/ -O ./source/resource_2 https://raw.githubusercontent.com/wiki/Peeragogy/Peeragogy.github.io/Monthly-Wrap:-April-2019-by-Charlie.md
sleep 3s; wget -P ./source/ -O ./source/resource_3 https://raw.githubusercontent.com/wiki/Peeragogy/Peeragogy.github.io/Monthly-Wrap:-May-2019-by-Charlie.md
sleep 3s; wget -P ./source/ -O ./source/resource_4 https://raw.githubusercontent.com/wiki/Peeragogy/Peeragogy.github.io/Monthly-Wrap:-January-2020-by-Charlie.mediawiki
sleep 3s; wget -P ./source/ -O ./source/resource_5 https://raw.githubusercontent.com/wiki/Peeragogy/Peeragogy.github.io/Monthly-Wrap:-February-2020.md
sleep 3s; wget -P ./source/ -O ./source/resource_6 https://raw.githubusercontent.com/wiki/Peeragogy/Peeragogy.github.io/Monthly-Wrap:-March-2020.md
sleep 3s; wget -P ./source/ -O ./source/resource_7 https://raw.githubusercontent.com/wiki/Peeragogy/Peeragogy.github.io/Monthly-Wrap:-April-to-September-2020.md

sleep 3s; wget -P ./media/ -O ./media/resource_0 https://upload.wikimedia.org/wikipedia/commons/c/c8/Thirty-five_rowers_on_a_long_racing_pirogue_in_Laos.jpg
sleep 3s; wget -P ./media/ -O ./media/resource_1 https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Boudhanath_2016-03-18.jpg/800px-Boudhanath_2016-03-18.jpg
sleep 3s; wget -P ./media/ -O ./media/resource_2 http://www.danoff.org/peeragogy-poster-feb-2020.jpeg
sleep 3s; wget -P ./media/ -O ./media/resource_3 http://danoff.org/peeragogy-march-web-change.png

mv ./media/resource_0 ./media/resource_0.jpg
mv ./media/resource_1 ./media/resource_1.jpg
mv ./media/resource_2 ./media/resource_2.jpg
mv ./media/resource_3 ./media/resource_3.png
