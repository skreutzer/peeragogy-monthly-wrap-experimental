#!/bin/sh

#mkdir -p ./PeeragogyMonthlyWrap/html/
#
#for filepath in ./PeeragogyMonthlyWrap/meeting-wraps/*.md
#do
#    # Only file name.
#    file=${filepath##*/}
#    filename=${file%.*}
#
#    # Or "-f gfm" in case of a more recent Pandoc package.
#    pandoc --email-obfuscation=none -f markdown_github-autolink_bare_uris-auto_identifiers $filepath -o $filepath.html
#
#    mv $filepath.html ./PeeragogyMonthlyWrap/html/$filename.html
#done
#
#java -cp ./digital_publishing_workflow_tools/text_concatenator/text_concatenator_1/ text_concatenator_1 jobfile_text_concatenator_1_202005_working_group_wraps.xml resultinfo_text_concatenator_1_202005_working_group_wraps.xml
#java -cp ./digital_publishing_workflow_tools/text_concatenator/text_concatenator_1/ text_concatenator_1 jobfile_text_concatenator_1_202008_working_group_wraps.xml resultinfo_text_concatenator_1_202008_working_group_wraps.xml


mkdir -p ./html/
mkdir -p ./epub/temp/
mkdir -p ./latex_1/
mkdir -p ./latex_2/

for filepath in ./source/resource_*
do
    # Only file name.
    file=${filepath##*/}

    if [ "$file" = "resource_4" ];
    then
        # Or "-f gfm" in case of a more recent Pandoc package.
        pandoc --email-obfuscation=none -f mediawiki-autolink_bare_uris-auto_identifiers $filepath -o $filepath.tmp.html
    else
        # Or "-f gfm" in case of a more recent Pandoc package.
        pandoc --email-obfuscation=none -f markdown_github-autolink_bare_uris-auto_identifiers $filepath -o $filepath.tmp.html
    fi

    java -cp ./automated_digital_publishing/txtreplace/txtreplace1/ txtreplace1 $filepath.tmp.html ./txtreplace1_replacement_dictionary.xml $filepath.tmp.html

    mv $filepath.tmp.html $filepath.html
    mv $filepath.html ./html/
done


#printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./jobfile_text_concatenator_1.xml
#printf "<text-concatenator-1-job>\n" >> ./jobfile_text_concatenator_1.xml
#printf "  <input>\n" >> ./jobfile_text_concatenator_1.xml
#printf "    <text-file path=\"./html/resource_7.html\"/>\n" >> ./jobfile_text_concatenator_1.xml
#printf "    <text-file path=\"./xhtml_pre_wrap_working_group_container.txt\"/>\n" >> ./jobfile_text_concatenator_1.xml
#printf "    <text-file path=\"./PeeragogyMonthlyWrap/html/peeragogy-monthly-wrap-working-group-meeting-wrap_202005.html\"/>\n" >> ./jobfile_text_concatenator_1.xml
#printf "    <text-file path=\"./PeeragogyMonthlyWrap/html/peeragogy-monthly-wrap-working-group-meeting-wrap_202008.html\"/>\n" >> ./jobfile_text_concatenator_1.xml
#printf "    <text-file path=\"./xhtml_post_wrap_working_group_container.txt\"/>\n" >> ./jobfile_text_concatenator_1.xml
#printf "  </input>\n" >> ./jobfile_text_concatenator_1.xml
#printf "  <output-file path=\"./html/resource_7.new.html\"/>\n" >> ./jobfile_text_concatenator_1.xml
#printf "</text-concatenator-1-job>\n" >> ./jobfile_text_concatenator_1.xml
#
#java -cp ./digital_publishing_workflow_tools/text_concatenator/text_concatenator_1/ text_concatenator_1 jobfile_text_concatenator_1.xml resultinfo_text_concatenator_1_1.xml
#rm jobfile_text_concatenator_1.xml
#
#mv ./html/resource_7.new.html ./html/resource_7.html


printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./jobfile_text_concatenator_1.xml
printf "<text-concatenator-1-job>\n" >> ./jobfile_text_concatenator_1.xml
printf "  <input>\n" >> ./jobfile_text_concatenator_1.xml

for filepath in ./html/*
do
    printf "    <text-file path=\"%s\"/>\n" $filepath >> ./jobfile_text_concatenator_1.xml
done

printf "  </input>\n" >> ./jobfile_text_concatenator_1.xml
printf "  <output-file path=\"./html/all.html\"/>\n" >> ./jobfile_text_concatenator_1.xml
printf "</text-concatenator-1-job>\n" >> ./jobfile_text_concatenator_1.xml

java -cp ./digital_publishing_workflow_tools/text_concatenator/text_concatenator_1/ text_concatenator_1 jobfile_text_concatenator_1.xml resultinfo_text_concatenator_1_2.xml
rm jobfile_text_concatenator_1.xml


for filepath in ./html/*
do
    printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./jobfile_text_concatenator_1.xml
    printf "<text-concatenator-1-job>\n" >> ./jobfile_text_concatenator_1.xml
    printf "  <input>\n" >> ./jobfile_text_concatenator_1.xml
    printf "    <text-file path=\"./xhtml_pre.txt\"/>\n" >> ./jobfile_text_concatenator_1.xml
    printf "    <text-file path=\"%s\"/>\n" $filepath >> ./jobfile_text_concatenator_1.xml
    printf "    <text-file path=\"./xhtml_post.txt\"/>\n" >> ./jobfile_text_concatenator_1.xml
    printf "  </input>\n" >> ./jobfile_text_concatenator_1.xml
    printf "  <output-file path=\"%s.tmp\"/>\n" $filepath >> ./jobfile_text_concatenator_1.xml
    printf "</text-concatenator-1-job>\n" >> ./jobfile_text_concatenator_1.xml

    java -cp ./digital_publishing_workflow_tools/text_concatenator/text_concatenator_1/ text_concatenator_1 jobfile_text_concatenator_1.xml resultinfo_text_concatenator_1_3.xml

    rm jobfile_text_concatenator_1.xml
    rm $filepath
    mv $filepath.tmp $filepath
done


#printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./jobfile_xml_xslt_transformator_1_working_group_wraps.xml
#printf "<xml-xslt_transformator-1-jobfile>\n" >> ./jobfile_xml_xslt_transformator_1_working_group_wraps.xml
#printf "  <job input-file=\"./html/resource_7.html\" entities-resolver-config-file=\"./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/entities/config_xhtml_1_0_strict.xml\" stylesheet-file=\"./working_group_wraps_to_monthly_wrap.xsl\" output-file=\"./html/resource_7.new.html\"/>\n" >> ./jobfile_xml_xslt_transformator_1_working_group_wraps.xml
#printf "  <job input-file=\"./html/all.html\" entities-resolver-config-file=\"./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/entities/config_xhtml_1_0_strict.xml\" stylesheet-file=\"./working_group_wraps_to_monthly_wrap.xsl\" output-file=\"./html/all.new.html\"/>\n" >> ./jobfile_xml_xslt_transformator_1_working_group_wraps.xml
#printf "</xml-xslt_transformator-1-jobfile>\n" >> ./jobfile_xml_xslt_transformator_1_working_group_wraps.xml
#
#java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 jobfile_xml_xslt_transformator_1_working_group_wraps.xml resultinfo_xml_xslt_transformator_1_working_group_wraps.xml
#rm jobfile_xml_xslt_transformator_1_working_group_wraps.xml
#
#mv ./html/resource_7.new.html ./html/resource_7.html
#mv ./html/all.new.html ./html/all.html


java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wraps.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_201812.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_201812.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_201903.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_201903.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_201904.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_201904.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_201905.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_201905.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_202001.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_202001.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_202002.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_202002.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_202003.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_202003.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./jobfile_html2epub2_202009.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy_monthly_wrap_202009.epub
rm -rf ./epub/temp/*


for filepath in ./html/*
do
    # Only file name.
    file=${filepath##*/}
    filename=${file%.*}

    printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./jobfile_xhtml_to_latex_1.xml
    printf "<xhtml-to-latex-1-workflow-jobfile>\n" >> ./jobfile_xhtml_to_latex_1.xml
    printf "  <input-file path=\"./html/%s\"/>\n" $file >> ./jobfile_xhtml_to_latex_1.xml

    if [ "$filename" = "all" ];
    then
        printf "  <stylesheet-file path=\"./peeragogy_periodical_1.xsl\"/>\n" >> ./jobfile_xhtml_to_latex_1.xml
    else
        printf "  <stylesheet-file path=\"./peeragogy_periodical_2.xsl\"/>\n" >> ./jobfile_xhtml_to_latex_1.xml
    fi

    printf "  <output-file path=\"./latex_1/%s.tmp.tex\"/>\n" $filename >> ./jobfile_xhtml_to_latex_1.xml
    printf "</xhtml-to-latex-1-workflow-jobfile>\n" >> ./jobfile_xhtml_to_latex_1.xml

    java -cp ./digital_publishing_workflow_tools/workflows/xhtml_to_latex/xhtml_to_latex_1/ xhtml_to_latex_1 jobfile_xhtml_to_latex_1.xml resultinfo_xhtml_to_latex_1.xml

    rm jobfile_xhtml_to_latex_1.xml

    java -cp ./automated_digital_publishing/txtreplace/txtreplace1/ txtreplace1 ./latex_1/$filename.tmp.tex ./txtreplace1_replacement_dictionary_latex.xml ./latex_1/$filename.tex
    rm ./latex_1/$filename.tmp.tex

    cd ./latex_1/

    xelatex $filename.tex
    xelatex $filename.tex
    makeindex $filename.idx
    xelatex $filename.tex
    xelatex $filename.tex

    cd ..
done

for filepath in ./html/*
do
    # Only file name.
    file=${filepath##*/}
    filename=${file%.*}

    printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./jobfile_xhtml_to_latex_1.xml
    printf "<xhtml-to-latex-1-workflow-jobfile>\n" >> ./jobfile_xhtml_to_latex_1.xml
    printf "  <input-file path=\"./html/%s\"/>\n" $file >> ./jobfile_xhtml_to_latex_1.xml

    if [ "$filename" = "all" ];
    then
        printf "  <stylesheet-file path=\"./tufte_periodical_1.xsl\"/>\n" >> ./jobfile_xhtml_to_latex_1.xml
    else
        printf "  <stylesheet-file path=\"./tufte_periodical_2.xsl\"/>\n" >> ./jobfile_xhtml_to_latex_1.xml
    fi

    printf "  <output-file path=\"./latex_2/%s.tmp.tex\"/>\n" $filename >> ./jobfile_xhtml_to_latex_1.xml
    printf "</xhtml-to-latex-1-workflow-jobfile>\n" >> ./jobfile_xhtml_to_latex_1.xml

    java -cp ./digital_publishing_workflow_tools/workflows/xhtml_to_latex/xhtml_to_latex_1/ xhtml_to_latex_1 jobfile_xhtml_to_latex_1.xml resultinfo_xhtml_to_latex_1.xml

    rm jobfile_xhtml_to_latex_1.xml

    java -cp ./automated_digital_publishing/txtreplace/txtreplace1/ txtreplace1 ./latex_2/$filename.tmp.tex ./txtreplace1_replacement_dictionary_latex.xml ./latex_2/$filename.tex
    rm ./latex_2/$filename.tmp.tex

    cd ./latex_2/

    xelatex $filename.tex
    xelatex $filename.tex
    makeindex $filename.idx
    xelatex $filename.tex
    xelatex $filename.tex

    cd ..
done
