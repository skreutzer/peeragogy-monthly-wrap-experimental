<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xhtml">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

  <xsl:template match="@*|node()|text()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()|text()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="xhtml:html/xhtml:body//xhtml:div[@class='peeragogy-monthly-wrap-working-group-meeting-wrap']">
    <h2>“Peeragogy Monthly Wrap” Working Group Meeting Wrap</h2>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="xhtml:html/xhtml:body//xhtml:div[@class='peeragogy-monthly-wrap-working-group-meeting-wrap']//xhtml:h1">
    <h3>
      <xsl:for-each select="@*">
        <xsl:copy-of select="."/>
      </xsl:for-each>
      <xsl:apply-templates/>
    </h3>
  </xsl:template>

  <xsl:template match="xhtml:html/xhtml:body//xhtml:div[@class='peeragogy-monthly-wrap-working-group-meeting-wrap']//xhtml:h2">
    <h4>
      <xsl:for-each select="@*">
        <xsl:copy-of select="."/>
      </xsl:for-each>
      <xsl:apply-templates/>
    </h4>
  </xsl:template>

  <xsl:template match="xhtml:html/xhtml:body//xhtml:div[@class='peeragogy-monthly-wrap-working-group-meeting-wrap']//xhtml:h3">
    <h5>
      <xsl:for-each select="@*">
        <xsl:copy-of select="."/>
      </xsl:for-each>
      <xsl:apply-templates/>
    </h5>
  </xsl:template>

  <xsl:template match="xhtml:html/xhtml:body//xhtml:div[@class='peeragogy-monthly-wrap-working-group-meeting-wrap']//xhtml:h4">
    <h6>
      <xsl:for-each select="@*">
        <xsl:copy-of select="."/>
      </xsl:for-each>
      <xsl:apply-templates/>
    </h6>
  </xsl:template>

  <xsl:template match="xhtml:html/xhtml:body//xhtml:div[@class='peeragogy-monthly-wrap-working-group-meeting-wrap']//xhtml:h5">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="xhtml:html/xhtml:body//xhtml:div[@class='peeragogy-monthly-wrap-working-group-meeting-wrap']//xhtml:h6">
    <xsl:apply-templates/>
  </xsl:template>

</xsl:stylesheet>
