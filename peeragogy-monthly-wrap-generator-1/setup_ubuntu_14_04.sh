#!/bin/sh

mkdir ./pgf/
cd ./pgf/
wget https://downloads.sourceforge.net/project/pgf/pgf/version%203.1.1/pgf_3.1.1.tds.zip
unzip pgf_3.1.1.tds.zip
rm pgf_3.1.1.tds.zip
cd ..
mkdir ~/texmf/
mv ./pgf/* ~/texmf/
rm -rf ./pgf/


mkdir ./xurl/
cd ./xurl/
wget http://mirrors.ctan.org/macros/latex/contrib/xurl.zip
unzip xurl.zip
rm xurl.zip
mv ./xurl/ ~/texmf/tex/latex/
cd ..
rm -rf ./xurl/
