<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright 2007–2021 by Kevin Godby, Bil Kleb, Bill Wood, Stephan Kreutzer

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml">
  <xsl:output method="text" encoding="UTF-8"/>

  <xsl:template match="/">
    <xsl:text>% This file was created by tufte_periodical_2.xsl, which is licensed under the Apache License 2.0.&#xA;&#xA;</xsl:text>
    <xsl:text>\documentclass[twoside,symmetric,justified]{tufte-handout} % Also supports option a4paper.&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>% https://tex.stackexchange.com/questions/202142/problems-compiling-tufte-title-page-in-xelatex&#xA;</xsl:text>
    <xsl:text>\usepackage{ifxetex}&#xA;</xsl:text>
    <xsl:text>\ifxetex&#xA;</xsl:text>
    <xsl:text>  \newcommand{\textls}[2][5]{%&#xA;</xsl:text>
    <xsl:text>    \begingroup\addfontfeatures{LetterSpace=#1}#2\endgroup&#xA;</xsl:text>
    <xsl:text>  }&#xA;</xsl:text>
    <xsl:text>  \renewcommand{\allcapsspacing}[1]{\textls[15]{#1}}&#xA;</xsl:text>
    <xsl:text>  \renewcommand{\smallcapsspacing}[1]{\textls[10]{#1}}&#xA;</xsl:text>
    <xsl:text>  \renewcommand{\allcaps}[1]{\textls[15]{\MakeTextUppercase{#1}}}&#xA;</xsl:text>
    <xsl:text>  \renewcommand{\smallcaps}[1]{\smallcapsspacing{\scshape\MakeTextLowercase{#1}}}&#xA;</xsl:text>
    <xsl:text>  \renewcommand{\textsc}[1]{\smallcapsspacing{\textsmallcaps{#1}}}&#xA;</xsl:text>
    <xsl:text>  \usepackage{fontspec}&#xA;</xsl:text>
    <xsl:text>\fi&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\usepackage{fontspec}&#xA;</xsl:text>
    <xsl:text>\setmainfont{ETBembo}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:choose>
      <xsl:when test="./xhtml:html/xhtml:head/xhtml:title">
        <xsl:text>\title{</xsl:text>
        <xsl:value-of select="/xhtml:html/xhtml:head/xhtml:title//text()"/>
        <xsl:text>}&#xA;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>%\title{}&#xA;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>%\author[]{}&#xA;</xsl:text>
    <xsl:text>\date{}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>%\geometry{showframe} % display margins for debugging page layout&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\usepackage{graphicx} % allow embedded images&#xA;</xsl:text>
    <xsl:text>  \setkeys{Gin}{width=\linewidth,totalheight=\textheight,keepaspectratio}&#xA;</xsl:text>
    <xsl:text>  \graphicspath{{graphics/}} % set of paths to search for images&#xA;</xsl:text>
    <xsl:text>\usepackage{amsmath}  % extended mathematics&#xA;</xsl:text>
    <xsl:text>\usepackage{booktabs} % book-quality tables&#xA;</xsl:text>
    <xsl:text>\usepackage{units}    % non-stacked fractions and better unit spacing&#xA;</xsl:text>
    <xsl:text>\usepackage{multicol} % multiple column layout facilities&#xA;</xsl:text>
    <xsl:text>\usepackage{fancyvrb} % extended verbatim environments&#xA;</xsl:text>
    <xsl:text>  \fvset{fontsize=\normalsize} % default font size for fancy-verbatim environments&#xA;</xsl:text>
    <xsl:text>\usepackage{xurl} % hard-wrap URLs in the side margins&#xA;</xsl:text>
    <xsl:text>\usepackage{mparhack} % put margin notes onto the correct side for twoside layout&#xA;</xsl:text>
    <xsl:text>\usepackage{morefloats}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\raggedbottom&#xA;</xsl:text>
    <xsl:text>\maxdeadcycles=1000&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>% Generates the index&#xA;</xsl:text>
    <xsl:text>\usepackage{makeidx}&#xA;</xsl:text>
    <xsl:text>\makeindex&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\begin{document}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\maketitle% this prints the handout title, author, and date&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>%\printclassoptions&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\bibliography{\jobname}&#xA;</xsl:text>
    <xsl:text>\bibliographystyle{plainnat}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\newpage&#xA;</xsl:text>
    <xsl:text>\begin{fullwidth}&#xA;</xsl:text>
    <xsl:text>~\vfill&#xA;</xsl:text>
    <xsl:text>\thispagestyle{empty}&#xA;</xsl:text>
    <xsl:text>\setlength{\parindent}{0pt}&#xA;</xsl:text>
    <xsl:text>\setlength{\parskip}{\baselineskip}&#xA;</xsl:text>
    <xsl:text>Layout: Copyright \copyright\ 2007-\the\year\ Kevin Godby, Bil Kleb, Bill Wood, Stephan Kreutzer&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\par The layout is licensed under the Apache License, Version 2.0 (the “License”); you may not&#xA;</xsl:text>
    <xsl:text>use this layout except in compliance with the License. You may obtain a copy&#xA;</xsl:text>
    <xsl:text>of the License at \url{http://www.apache.org/licenses/LICENSE-2.0}. Unless&#xA;</xsl:text>
    <xsl:text>required by applicable law or agreed to in writing, layouts distributed&#xA;</xsl:text>
    <xsl:text>under the License are distributed on an “AS IS” BASIS, WITHOUT&#xA;</xsl:text>
    <xsl:text>WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the&#xA;</xsl:text>
    <xsl:text>License for the specific language governing permissions and limitations&#xA;</xsl:text>
    <xsl:text>under the License.&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>“ET Book” font: Copyright \copyright\ 2015-\the\year\ Dmitry Krasny, Bonnie Scranton, Edward Tufte&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>Permission is hereby granted, free of charge, to any person obtaining a copy of the “ET Book” font and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\\&#xA;</xsl:text>
    <xsl:text>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\\&#xA;</xsl:text>
    <xsl:text>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.&#xA;</xsl:text>
    <xsl:text>\end{fullwidth}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\end{document}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:p">
    <xsl:if test="@class='copyright-notice'">
      <xsl:text>\begin{footnotesize}&#xA;</xsl:text>
    </xsl:if>
    <xsl:apply-templates/>
    <xsl:if test="@class='copyright-notice'">
      <xsl:text>\end{footnotesize}&#xA;</xsl:text>
    </xsl:if>
    <!-- Default TeX paragraph. -->
    <xsl:text>&#xA;&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:p//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h1">
    <xsl:text>\setcounter{footnote}{0}&#xA;</xsl:text>
    <xsl:text>\section{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h1//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h2">
    <xsl:text>\subsection{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h2//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h3">
    <xsl:text>\paragraph{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h3//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h4">
    <xsl:text>\begin{center}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{center}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h4//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h5">
    <xsl:text>\begin{center}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\end{center}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h5//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h6">
    <xsl:text>\begin{center}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\end{center}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h6//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:ul">
    <xsl:text>\begin{itemize}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{itemize}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:ol">
    <xsl:text>\begin{enumerate}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{enumerate}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:li">
    <xsl:text>\item </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:li//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:a">
    <xsl:choose>
      <xsl:when test="@href">
        <xsl:apply-templates/>
        <xsl:text>\sidenote{\url{</xsl:text>
        <xsl:value-of select="@href"/>
        <xsl:text>}}</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:a//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:img">
    <xsl:text>\begin{center}&#xA;</xsl:text>
    <xsl:text>\includegraphics[width=1\linewidth]{</xsl:text>
    <xsl:value-of select="@src"/>
    <xsl:text>}&#xA;</xsl:text>
    <xsl:text>\end{center}&#xA;\noindent{}\ignorespacesafterend{}\ifhmode\unskip\fi</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:blockquote">
    <xsl:text>\begin{quote}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{quote}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:blockquote//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="text()|@*"/>

</xsl:stylesheet>
