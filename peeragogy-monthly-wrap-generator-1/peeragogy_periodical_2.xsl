<?xml version="1.0" encoding="UTF-8"?>
<!--
Original copyright (C) 2013-2021 by Joe Corneli, Stephan Kreutzer waived
via Creative Commons Zero 1.0 Universal Public Domain Dedication.
-->
<!-- This file is based on peeragogy-shell.tex of the Peeragogy Handbook (see https://github.com/Peeragogy/peeragogy-handbook/blob/master/en/peeragogy-shell.tex and http://peeragogy.org). -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml">
  <xsl:output method="text" encoding="UTF-8"/>

  <xsl:template match="/">
    <xsl:text>% This file was created by peeragogy_periodical_2.xsl, which was released via Creative Commons Zero 1.0 Universal Public Domain Dedication.&#xA;&#xA;</xsl:text>
    <xsl:text>% The "ebook" option used here selects a paper size of 6x9 inches.&#xA;</xsl:text>
    <xsl:text>\documentclass[ebook,12pt,twoside]{memoir}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>%% To remove pictures, uncomment this line&#xA;</xsl:text>
    <xsl:text>%\PassOptionsToPackage{draft}{graphicx}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\renewcommand*{\cftchapterfont}{\bfseries}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\makeatletter&#xA;</xsl:text>
    <xsl:text>\def\below{\xdef\@thefnmark{}\@footnotetext}&#xA;</xsl:text>
    <xsl:text>\makeatother&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\usepackage{fontspec}&#xA;</xsl:text>
    <xsl:text>\usepackage{xunicode}&#xA;</xsl:text>
    <xsl:text>\defaultfontfeatures{Mapping=tex-text}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\setmainfont[Mapping=tex-text,&#xA;</xsl:text>
    <xsl:text>     SmallCapsFont={Linux Libertine O},&#xA;</xsl:text>
    <xsl:text>     SmallCapsFeatures= {Color=FFFFFF, RawFeature={+smcp,-hlig,-dlig}},&#xA;</xsl:text>  
    <xsl:text>     BoldFont={Linux Biolinum O},&#xA;</xsl:text>
    <xsl:text>%     BoldFeatures={Color = FFFFFF,SmallCapsFont={Linux Libertine Capitals O Bold},%&#xA;</xsl:text>
    <xsl:text>%       SmallCapsFeatures = { Color=FFFFFF,   RawFeature={+smcp,+hlig,+dlig}} },&#xA;</xsl:text>
    <xsl:text>     ItalicFont={Linux Libertine O},&#xA;</xsl:text>
    <xsl:text>     ItalicFeatures={Color = FFFFFF,%&#xA;</xsl:text>
    <xsl:text>       SmallCapsFont={Linux Libertine O},%&#xA;</xsl:text>
    <xsl:text>       SmallCapsFeatures = {Color=FFFFFF,&#xA;</xsl:text>
    <xsl:text>                            Ligatures={NoCommon}},&#xA;</xsl:text>
    <xsl:text>                            Numbers=OldStyle},&#xA;</xsl:text>
    <xsl:text>     BoldItalicFont={Linux Libertine O},&#xA;</xsl:text>
    <xsl:text>     BoldItalicFeatures={ Color = FFFFFF,%&#xA;</xsl:text>
    <xsl:text>      SmallCapsFont={Linux Libertine O},%&#xA;</xsl:text>
    <xsl:text>      SmallCapsFeatures = { Color=FFFFFF,&#xA;</xsl:text>
    <xsl:text>                            RawFeature={+smcp,-hlig,-dlig}}} ]{Linux Libertine O}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\let\sc\scshape&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\usepackage{tikz}&#xA;</xsl:text>
    <xsl:text>\usetikzlibrary{calc}&#xA;</xsl:text>
    <xsl:text>\usetikzlibrary{positioning,backgrounds,fit,arrows,arrows.meta,shapes,shadows}&#xA;</xsl:text>
    <xsl:text>\usetikzlibrary{shapes.multipart}&#xA;</xsl:text>
    <xsl:text>\usetikzlibrary{intersections}&#xA;</xsl:text>
    <xsl:text>\usepackage{pgflibraryarrows}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\usepackage{polyglossia}&#xA;</xsl:text>
    <xsl:text>\setmainlanguage[variant=american]{english}&#xA;</xsl:text>
    <xsl:text>\PolyglossiaSetup{english}{indentfirst=false}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\usepackage{marvosym}&#xA;</xsl:text>
    <xsl:text>\usepackage{datetime}&#xA;</xsl:text>
    <xsl:text>\let\dateseparator.&#xA;</xsl:text>
    <xsl:text>\ddmmyyyydate&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\usepackage{graphicx}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\usepackage{enumerate}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\renewcommand*{\insertchapterspace}{%&#xA;</xsl:text>
    <xsl:text>  \addtocontents{lof}{\protect\addvspace{3pt}}%&#xA;</xsl:text>
    <xsl:text>  \addtocontents{lot}{\protect\addvspace{3pt}}}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <!-- \aliaspagestyle{part}{empty} -->
    <!-- \renewcommand{\thepart}{\Roman{part}} -->
    <xsl:text>\chapterstyle{thatcher}&#xA;</xsl:text>
    <xsl:text>\renewcommand*{\printchaptername}{}&#xA;</xsl:text>
    <xsl:text>\renewcommand*{\afterchapternum}{}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\let\sc\scshape&#xA;</xsl:text>
    <xsl:text>\let\bf\bfseries&#xA;</xsl:text>
    <xsl:text>\let\it\itfamily&#xA;</xsl:text>
    <xsl:text>\let\tt\ttfamily&#xA;</xsl:text>
    <xsl:text>\let\sl\slshape&#xA;</xsl:text>
    <xsl:text>\let\sf\sffamily&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>%% Adjust trim margins where the "ebook" memoir option selected a target paper size of 6x9 inches:&#xA;</xsl:text>
    <xsl:text>%\settrimmedsize{9in}{6in}{*}&#xA;</xsl:text>
    <xsl:text>%\settrims{1in}{1.25in}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\setlength{\marginparsep}{0pt}&#xA;</xsl:text>
    <xsl:text>\setlength{\marginparwidth}{0pt}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>% Redefined to be without numbers in the running page header.&#xA;</xsl:text>
    <xsl:text>\renewcommand{\chaptermark}[1]{\markboth{{#1}}{}}&#xA;</xsl:text>
    <xsl:text>\renewcommand{\sectionmark}[1]{\markboth{}{{#1}}}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>% Redefined to be without numbers in the headers.&#xA;</xsl:text>
    <xsl:text>\renewcommand{\thechapter}{}&#xA;</xsl:text>
    <xsl:text>\renewcommand{\thesection}{}&#xA;</xsl:text>
    <xsl:text>\renewcommand{\thesubsection}{}&#xA;</xsl:text>
    <xsl:text>\renewcommand{\thesubsubsection}{}&#xA;</xsl:text>
    <xsl:text>\renewcommand{\theparagraph}{}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\makeevenhead{companion}{\normalfont\bfseries\scshape\thepage}{}%&#xA;</xsl:text>
    <xsl:text>                        {\normalfont\large\bfseries\scshape\leftmark}&#xA;</xsl:text>
    <xsl:text>\makeoddhead{companion}{\normalfont\large\bfseries\scshape\rightmark}{}%&#xA;</xsl:text>
    <xsl:text>                       {\normalfont\bfseries\scshape\thepage}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\settypeblocksize{7.4in}{4.4in}{*}&#xA;</xsl:text>
    <xsl:text>\setlrmargins{1in}{*}{*}&#xA;</xsl:text>
    <xsl:text>\setulmargins{.9in}{*}{*}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\setheadfoot{2\onelineskip}{2\onelineskip}&#xA;</xsl:text>
    <xsl:text>\setheaderspaces{*}{1.25\onelineskip}{*}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\checkandfixthelayout&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\PassOptionsToPackage{hyphens}{url}&#xA;</xsl:text>
    <xsl:text>\usepackage[linktoc=none,frenchlinks,hyperindex=false,pdfborderstyle={/S/U/W .5},citebordercolor={1 1 1},linkbordercolor={1 1 1},urlbordercolor={1 1 1}]{hyperref}&#xA;</xsl:text>
    <xsl:text>\renewcommand{\hyperpage}[1]{}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>% Potentially dangerous way to render "|hyperpage" of the *.ind file ineffective.&#xA;</xsl:text>
    <xsl:text>\usepackage{filecontents}&#xA;</xsl:text>
    <xsl:text>\begin{filecontents}{\jobname.mst}&#xA;</xsl:text>
    <xsl:text>delim_0 ""&#xA;</xsl:text>
    <xsl:text>delim_1 ""&#xA;</xsl:text>
    <xsl:text>delim_2 ""&#xA;</xsl:text>
    <xsl:text>suffix_1p ""&#xA;</xsl:text>
    <xsl:text>suffix_2p ""&#xA;</xsl:text>
    <xsl:text>suffix_3p ""&#xA;</xsl:text>
    <xsl:text>\end{filecontents}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\makeatletter&#xA;</xsl:text>
    <xsl:text>\g@addto@macro{\UrlBreaks}{\UrlOrds}&#xA;</xsl:text>
    <xsl:text>\makeatother&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\newcounter{IndexItemCounter}&#xA;</xsl:text>
    <xsl:text>\usepackage{makeidx}&#xA;</xsl:text>
    <xsl:text>\usepackage{xstring}&#xA;</xsl:text>
    <xsl:text>\makeindex&#xA;</xsl:text>
    <xsl:text>\let\hrefold\href&#xA;</xsl:text>
    <xsl:text>\newcommand*{\hrefnew}[2]{%&#xA;</xsl:text>
    <xsl:text>  \normalexpandarg%&#xA;</xsl:text>
    <xsl:text>  \IfStrEq{#1}{#2}{\textsc{#2}}{%&#xA;</xsl:text>
    <xsl:text>    \hrefold{#1}{#2}%&#xA;</xsl:text>
    <xsl:text>    \stepcounter{IndexItemCounter}%&#xA;</xsl:text>
    <xsl:text>    \index{\thepage@{Page \thepage}!{\theIndexItemCounter}@{#2}!\theIndexItemCounter@{\url{#1}}}\unskip%&#xA;</xsl:text>
    <xsl:text>  }%&#xA;</xsl:text>
    <xsl:text>}&#xA;</xsl:text>
    <xsl:text>\let\href\hrefnew&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\usepackage{accsupp}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\setlength{\parskip}{0pt}&#xA;</xsl:text>
    <xsl:text>\raggedbottom&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\begin{document}&#xA;</xsl:text>
    <xsl:text>\selectlanguage{english}&#xA;</xsl:text>
    <xsl:text>\sloppy&#xA;</xsl:text>
    <xsl:if test="./xhtml:html/xhtml:head/xhtml:title">
      <!--xsl:text>\part{</xsl:text>
      <xsl:value-of select="/xhtml:html/xhtml:head/xhtml:title//text()"/>
      <xsl:text>}&#xA;</xsl:text-->
    </xsl:if>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\begingroup&#xA;</xsl:text>
    <xsl:text>\renewcommand{\cleardoublepage}{}&#xA;</xsl:text>
    <xsl:text>\renewcommand{\clearpage}{}&#xA;</xsl:text>
    <xsl:text>\section{Web References}&#xA;</xsl:text>
    <xsl:text>\renewcommand{\indexname}{}&#xA;</xsl:text>
    <xsl:text>\vspace{-1in}&#xA;</xsl:text>
    <xsl:text>\onecolindex&#xA;</xsl:text>
    <xsl:text>\begingroup&#xA;</xsl:text>
    <!--xsl:text>\smaller&#xA;</xsl:text-->
    <xsl:text>\printindex&#xA;</xsl:text>
    <xsl:text>\endgroup&#xA;</xsl:text>
    <xsl:text>\endgroup&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\end{document}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:p">
    <xsl:if test="@class='copyright-notice'">
      <xsl:text>\begin{footnotesize}&#xA;</xsl:text>
    </xsl:if>
    <xsl:apply-templates/>
    <xsl:if test="@class='copyright-notice'">
      <xsl:text>\end{footnotesize}&#xA;</xsl:text>
    </xsl:if>
    <!-- Default TeX paragraph. -->
    <xsl:text>&#xA;&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:p//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h1">
    <xsl:text>\chapter{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h1//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h2">
    <xsl:text>\section{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h2//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h3">
    <xsl:text>\subsection{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h3//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h4">
    <xsl:text>\subsubsection{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h4//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h5">
    <xsl:text>\paragraph{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h5//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h6">
    <xsl:text>\begin{center}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\end{center}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h6//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:ul">
    <xsl:text>\begin{itemize}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{itemize}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:ol">
    <xsl:text>\begin{enumerate}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{enumerate}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:li">
    <xsl:text>\item </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:li//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:a">
    <xsl:choose>
      <xsl:when test="@href">
        <xsl:text>\href{</xsl:text>
        <xsl:value-of select="@href"/>
        <xsl:text>}{</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:a//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:img">
    <xsl:text>\begin{center}&#xA;</xsl:text>
    <xsl:text>\includegraphics[width=1\linewidth]{</xsl:text>
    <xsl:value-of select="@src"/>
    <xsl:text>}&#xA;</xsl:text>
    <xsl:text>\end{center}&#xA;\noindent{}\ignorespacesafterend{}\ifhmode\unskip\fi</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:blockquote">
    <xsl:text>\begin{quote}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{quote}&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:blockquote//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="text()|@*"/>

</xsl:stylesheet>
