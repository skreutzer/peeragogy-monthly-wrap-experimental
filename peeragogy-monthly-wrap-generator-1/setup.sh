#!/bin/sh
sudo apt-get install default-jre pandoc unzip wget texlive-xetex texlive-latex-recommended texlive-latex-extra fonts-linuxlibertine texlive-fonts-recommended

wget https://github.com/Tufte-LaTeX/tufte-latex/archive/master.zip
unzip ./master.zip
mv ./tufte-latex-master/ ./tufte-latex/
rm master.zip

sleep 3s; wget https://github.com/edwardtufte/tufte-css/archive/gh-pages.zip
unzip ./gh-pages.zip
mv ./tufte-css-gh-pages/ ./tufte-css/
rm ./gh-pages.zip

#sleep 3s; wget https://github.com/Peeragogy/PeeragogyMonthlyWrap/archive/master.zip
#unzip ./master.zip
#mv ./PeeragogyMonthlyWrap-master/ ./PeeragogyMonthlyWrap/
#rm ./master.zip

java -cp ./digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/ resource_retriever_1 jobfile_resource_retriever_1_source.xml resultinfo_resource_retriever_1_source.xml
java -cp ./digital_publishing_workflow_tools/workflows/resource_retriever/resource_retriever_1/ resource_retriever_1 jobfile_resource_retriever_1_media.xml resultinfo_resource_retriever_1_media.xml

mv ./media/resource_0 ./media/resource_0.jpg
mv ./media/resource_1 ./media/resource_1.jpg
mv ./media/resource_2 ./media/resource_2.jpg
mv ./media/resource_3 ./media/resource_3.png
